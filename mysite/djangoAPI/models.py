from django.db import models


# Create your models here.
class Patient(models.Model):
    last_name = models.CharField(max_length=40, blank=False, null=False)
    first_name = models.CharField(max_length=40, blank=False, null=False)
    email = models.EmailField(unique=True, blank=False, null=False)
    address = models.CharField(max_length=64, blank=False, null=False)


class Consultation(models.Model):
    CONSULTATION_TYPES = [
        ('E', 'Examination'),  # visite
        ('M', 'Monitoring'),  # suivi
        ('O', 'Operation'),  # operation
    ]
    date = models.DateField(blank=False, null=False)
    patient = models.ForeignKey(Patient, on_delete=models.CASCADE, blank=False, null=False)
    title = models.CharField(max_length=40, blank=False, null=False)
    description = models.CharField(max_length=200, blank=True, null=True)
    type = models.CharField(max_length=1, choices=CONSULTATION_TYPES, default='E', blank=False, null=False)
